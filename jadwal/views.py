from django.shortcuts import render, redirect
from django.http import HttpResponse
from .forms import JadwalForm
from .models import Jadwal
from django.utils import timezone
import datetime

# Create your views here.
def create_jadwal(request):
    form = JadwalForm()
    if request.method =="POST":
        form = JadwalForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('jadwal')
    return render(request, "form.html", {'form':form})

def jadwal(request):
    jadwals = Jadwal.objects.order_by("tanggal", "waktu")
    response = {
        'jadwal' : jadwals
    }
    return render(request, "jadwal.html", response)

def delete(request):
    if request.method == "POST":
        id = request.POST['id']
        Jadwal.objects.get(id=id).delete()
    return redirect('jadwal')
