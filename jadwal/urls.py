from django.contrib import admin
from django.urls import path
from . import views

app_name='jadwal'
urlpatterns = [
    path('create', views.create_jadwal, name="create"),
    path('', views.jadwal, name='jadwal'),
    path('delete', views.delete)
]
